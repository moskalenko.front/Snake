class gameSnake {
   constructor(){
      this.area = new Area(300, 300);
      this.snake = new Snake('Vasya');
      this.point = new Point(this.area);
      this.speed = 200;
      this.interval;
      this.direction = 40;

      this.snake.init(this.area); 
      this.point.init();
   }
   checkPosition() {
      let tmpCoordinates = [];

      this.snake.snake.forEach((el, index) => {
         tmpCoordinates.push({
            x: el.x,
            y: el.y
         });        
      });

      if(this.direction === 38){
         this.snake.snake[0].y = this.snake.snake[0].y - this.snake.height;
         if(this.snake.snake[0].y < 0) {
            return this.stopGame();
         }
      } else if (this.direction === 40) {
         this.snake.snake[0].y = this.snake.snake[0].y + this.snake.height;
         if(this.snake.snake[0].y > this.area.height - this.snake.height) {
            return this.stopGame();
         }
      } else if (this.direction === 37) {  
         this.snake.snake[0].x = this.snake.snake[0].x - this.snake.width;
         if(this.snake.snake[0].x < 0) {
            return this.stopGame();
         }
      } else if (this.direction === 39) {
         this.snake.snake[0].x = this.snake.snake[0].x + this.snake.width;
         if(this.snake.snake[0].x > this.area.width - this.snake.width) {
            return this.stopGame();
         }
      }

      this.snake.snake.forEach((el, index) => {
         if(index !== 0){
            el.x = tmpCoordinates[index - 1].x,
            el.y = tmpCoordinates[index - 1].y
         }       
      });

      if(
         parseInt(this.point.point.style.top) === this.snake.snake[0].y 
         &&
         parseInt(this.point.point.style.left) === this.snake.snake[0].x 
      ){
         this.point.movePoint()
         this.snake.addLength(tmpCoordinates[tmpCoordinates.length - 1], this.area);
      }
      this.snake.move();
   }
   startGame() {
      document.addEventListener('keyup', (e) => {
         this.direction = e.keyCode;
      });
      this.interval = setInterval(() => {
         this.checkPosition();
      },this.speed)
   }
   stopGame() {
      clearInterval(this.interval);
   }
}
class Area {
   constructor(width, height){
      this.width = width;
      this.height = height;
      this.area = document.createElement('div');
      //init
      this.area.style.width = width + 'px';
      this.area.style.height = height + 'px';
      this.area.style.border = '2px solid #000';
      this.area.style.boxSizing = 'border-box';
      this.area.style.overflow = 'hidden';
      document.querySelector('body').appendChild(this.area);
   }
}
class Snake {
   constructor(name, area){
      this.name = name;
      this.width = 10;
      this.height = 10;
      this.bgColor = 'blue';
      this.snake = [];
   }
   init(area) {  
      this.addLength(null, area);  
   }
   move() {
      this.snake.forEach((el, index) => {
         el.elem.style.top = el.y + 'px';
         el.elem.style.left = el.x + 'px';
      });
   }
   addLength(coordinates, area){
      let elem = document.createElement('div');

      elem.style.width = this.width + 'px';
      elem.style.height = this.height + 'px';
      elem.style.backgroundColor = this.bgColor;
      elem.style.position = 'absolute';

      if (coordinates) {
         this.snake.push({
            elem: elem,
            x: coordinates.x,
            y: coordinates.y
         });

         elem.style.top = coordinates.y + 'px';
         elem.style.left = coordinates.x + 'px';
      } else {
         console.log('slon')
         this.snake.push({
            elem: elem,
            x: 0,
            y: 0
         });

         elem.style.top = '0px';
         elem.style.left = '0px';
      }
      area.area.appendChild(elem);
   }
}
class Point {
   constructor(area) {
      this.area = area;
      this.width = 10;
      this.height = 10;
      this.point = document.createElement('div');
   }
   random(max) {
      let rand = Math.random() * (max - 0) + 0;
      return Math.round(rand / 10) * 10;
   }
   init() {
      this.point.style.width = this.width + 'px';
      this.point.style.height = this.height + 'px';
      this.point.style.backgroundColor = 'red';
      this.point.style.position = 'absolute';
      this.area.area.appendChild(this.point);
      this.movePoint();
   }
   movePoint() {
      this.point.style.top = this.random(this.area.height - this.height) + 'px';
      this.point.style.left = this.random(this.area.width - this.width) + 'px';
   }
}

let game = new gameSnake();
game.startGame();
